const express = require("express")
const expressLayouts = require("express-ejs-layouts");

const app = express();
const port = process.env.PORT || 5000;

//EJS
app.use(expressLayouts);
app.set('view engine', 'ejs');

//Routes
app.use('/', require('./routes/index'));
app.use('/users', require('./routes/users'));

//jsut to check


app.listen(port, () => {
    console.log(`Server listning to port ${port}`)
})